<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Category List</title>
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> --}}
</head>
<body>
    <div class="container">
        @if(\Session::has('alert'))
        <div class="alert alert-success">
            <p>{{\Session::get('alert')}}</p>
        </div><br>
        @endif
        <h2>Kategori Produk</h2>
        <button class="btn btn-success" data-toggle="modal" data-target="#tambah">
            Tambah Kategori
        </button>
        <br/><br/>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>No</th>
                    <th colspan="3">Nama Kategori</th>
                </tr>
            </thead>
            <tbody>
                @foreach($categories as $cat)
                <tr>
                    <td>{{$cat->id}}</td>
                    <td>{{$cat->category_name}}</td>
                    <td align="right"><button class="btn btn-warning" data-toggle="modal" data-target="#edit_{{$cat->id}}">Edit</button></td>
                    <td align="left"><button class="btn btn-danger" data-toggle="modal" data-target="#delete_{{$cat->id}}">Delete</button></td>
                </tr>

                <div id="edit_{{$cat->id}}" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Ubah Kategori</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <form action="{{route('categories.update', $cat->id)}}" method="POST">
                                @csrf
                                    <input name="_method" type="hidden" value="PATCH">
                                    <div class="form-group">
                                        <label for="">Nama Kategori</label>
                                        <input type="text" name="cat_name" class="form-control" value="{{$cat->category_name}}" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info">Update</button> 
                                        <button data-dismiss="modal" class="btn btn-danger">Cancel</button> 
                                    </div>
                                </form>
                            </div>                    
                        </div>
                    </div>
                </div>

                <div id="delete_{{$cat->id}}" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Hapus Kategori</h5>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <form action="{{route('categories.destroy', $cat->id)}}" method="POST">
                                    @csrf
                                        <input name="_method" type="hidden" value="DELETE">
                                        <div class="form-group">
                                            <p>Anda Yakin akan MengHAPUS data?</p>
                                            <button type="submit" class="btn btn-info">HAPUS</button> 
                                            <button data-dismiss="modal" class="btn btn-danger">Cancel</button> 
                                        </div>
                                    </form>
                                </div>                    
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
    
        
        <!-- Modal(s) -->
        <div id="tambah" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Tambah Kategori</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('categories.store')}}" method="POST">
                        @csrf
                            <div class="form-group">
                                <label for="">Nama Kategori</label>
                                <input type="text" name="cat_name" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-info">Save</button> 
                                <button data-dismiss="modal" class="btn btn-danger">Cancel</button> 
                            </div>
                        </form>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</body>
</html>